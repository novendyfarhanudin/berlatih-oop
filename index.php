<?php
require("Animal.php");
require("Frog.php");
require("Ape.php");

$sheep = new Animal("shaun");
$buduk = new Frog("buduk");
$kera = new Ape("kera sakti");


echo "Name : " .$sheep->name . "<br>";
echo "legs : " .$sheep->legs . "<br>";
echo "cold blooded : " .$sheep->coldblood . "<br><br>";

echo "Name : " .$buduk->name . "<br>";
echo "legs : " .$buduk->legs . "<br>";
echo "cold blooded : " .$buduk->coldblood . "<br>";
echo "Jump            : " .$buduk->jump . "<br><br>";

echo "Name : " .$kera->name . "<br>";
echo "legs : " .$kera->legs . "<br>";
echo "cold blooded : " .$kera->coldblood . "<br>";
echo "Yell            : " .$kera->yell . "<br>";


?>